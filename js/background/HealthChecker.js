import Utils from './Utils.js';

const utils = new Utils();

export default function HealthChecker(steam, trader) {
    this.steam = steam;
    this.trader = trader;
	this._steamAccountIsAuthorized = false;
	this._traderAccountIsAuthorized = false;
	this._steamApiKeyIsRegistered = false;
	this._steamApiKeyIsSavedOnTraderAccount = false;
    this._lastCheckTime = null;
    this._minCheckFreqSec = 15;
}

HealthChecker.prototype.check = async function() {
    await this.checkSteam();
    await this.checkTrader();
};

HealthChecker.prototype.checkSteam = async function() {
    let now = Date.now();
    if(now - this._lastCheckTime < (this._minCheckFreqSec * 1000))
    {
        return false;
    }

    this._lastCheckTime = now;

    this._steamAccountIsAuthorized = await checkSteamAccountIsAuthorized(this);
    
    if(this._steamAccountIsAuthorized === false)
        return false;

    this._steamApiKeyIsRegistered = await checkSteamApiKeyIsRegistered(this);
    
    if(this._steamApiKeyIsRegistered === false){
        this._steamApiKeyIsRegistered = await RegisterSteamApiKey(this);
    }
};

HealthChecker.prototype.checkTrader = async function() {
    this._traderAccountIsAuthorized = await checkTraderAccountIsAuthorized(this);
    
    if(this._traderAccountIsAuthorized === false)
        return false;

    this._steamApiKeyIsSavedOnTraderAccount = await checkSteamApiKeyIsSavedOnTraderAccount(this);
    if(this._steamApiKeyIsSavedOnTraderAccount === false)
        this._steamApiKeyIsSavedOnTraderAccount = await SaveSteamApiKeyOnTraderAccount(this);
};

HealthChecker.prototype.allIsOk = function() {
	return (
			this._steamAccountIsAuthorized === true
		&&	this._steamApiKeyIsRegistered === true
		&&	this._traderAccountIsAuthorized === true
		&&	this._steamApiKeyIsSavedOnTraderAccount === true
	);
};

HealthChecker.prototype.getInfo = function() {
    return {
        'lastCheckTime': this._lastCheckTime,
        'all': this.allIsOk(),
        'steamAccountIsAuthorized':this._steamAccountIsAuthorized,
        'steamApiKeyIsRegistered':this._steamApiKeyIsRegistered,
        'traderAccountIsAuthorized':this._traderAccountIsAuthorized,
        'steamApiKeyIsSavedOnTraderAccount':this._steamApiKeyIsSavedOnTraderAccount
    };
};

/**
 * @return {boolean}
 */
async function checkSteamAccountIsAuthorized(self) {
	return await self.steam.isAccountAuthorized();
}

/**
 * @return {boolean}
 */
async function checkTraderAccountIsAuthorized(self) {
	return await self.trader.isAuthedWithSteamid(self.steam.accountSteamid);
}

/**
 * @return {boolean}
 */
async function checkSteamApiKeyIsRegistered(self) {
	return await self.steam.isApiKeyRegistered();
}

/**
 * @return {boolean}
 */
async function RegisterSteamApiKey(self) {
    return await self.steam.registerApiKey();
}

/**
 * @return {boolean}
 */
async function checkSteamApiKeyIsSavedOnTraderAccount(self) {
	return await self.trader.isSteamApiKeySaved(self.steam.accountApiKey);
}

/**
 * @return {boolean}
 */
async function SaveSteamApiKeyOnTraderAccount(self) {
    return await self.trader.saveSteamApiKeyOnTraderAccount(self.steam.accountApiKey);
}